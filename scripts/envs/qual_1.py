"""
Task environment SRCP2 Qual 1 round
"""

import sys
import os
import numpy as np
from numpy import pi
import rospy

from std_srvs.srv import Empty
from sensor_msgs.msg import LaserScan, Imu, Image, CompressedImage
from geometry_msgs.msg import Pose, Twist

class ResourceLocate:
    def __init__(self):
        rospy.init_node("resource_locate_continuous_env", anonymous=True, log_level=rospy.DEBUG)
        # env properties
        self.name = 'resource_locate_continuos'
        self.rate = rospy.Rate(30)
        self.max_episode_steps = int(30*45*60 )
        self.step_counter = 0
        self.observation_space_shape = (200,200,2) # left image + right image
        self.action_space_shape = (2,) # lin, ang
        self.action_space_high = np.array([5., pi/2])
        self.action_space_low = np.array([-5., -pi/2])
        # services
        self.reset_world_proxy = rospy.ServiceProxy('/gazebo/reset_world', Empty)
        self.unpause_physics_proxy = rospy.ServiceProxy('/gazebo/unpause_physics', Empty)
        self.pause_physics_proxy = rospy.ServiceProxy('/gazebo/pause_physics', Empty)
        # topic publisher
        self.cmd_vel_pub = rospy.Publisher("scout_1/skid_cmd_vel", Twist, queue_size=1)
        # subscribers
        # rospy.Subscriber("scout_1/laser/scan", LaserScan, self._laser_scan_callback) # get laser scan data
        # rospy.Subscriber("scout_1/imu", Imu, self._imu_callback) # get imu data
        # rospy.Subscriber("scout_1/camera/left/image_raw", Image, self._left_image_callback) # get image from left camera
        # rospy.Subscriber("scout_1/camera/left/image_raw/compressed", CompressedImage, self._left_image_compressed_callback) # get image from left camera
        # rospy.Subscriber("scout_1/camera/right/image_raw", Image, self._right_image_callback) # get image from right camera
        # rospy.Subscriber("scout_1/camera/right/image_raw", Image, self._right_image_callback) # get image from right camera

    def pausePhysics(self):
        rospy.wait_for_service("/gazebo/pause_physics")
        try:
            self.pause_physics_proxy()
        except rospy.ServiceException as e:
            rospy.logerr("/gazebo/pause_physics service call failed")

    def unpausePhysics(self):
        rospy.wait_for_service("/gazebo/unpause_physics")
        try:
            self.unpause_physics_proxy()
        except rospy.ServiceException as e:
            rospy.logerr("/gazebo/unpause_physics service call failed")

    def resetWorld(self):
        rospy.wait_for_service("/gazebo/reset_world")
        try:
            self.reset_world_proxy()
        except rospy.ServiceException as e:
            rospy.logerr("/gazebo/reset_world service call failed")

    def reset(self):
        """
        Reset environment
        Usage:
            obs = env.reset()
        """
        rospy.logdebug("\nStart Environment Reset")
        self.unpausePhysics()
        # zero cmd_vel
        zero_cmd_vel = Twist()
        self.cmd_vel_pub.publish(zero_cmd_vel)
        # set init pose
        self.pausePhysics()
        self.resetWorld()
        self.unpausePhysics()
        self.cmd_vel_pub.publish(zero_cmd_vel)
        # TODO:
        # add a module to test if scout touched ground
        #
        self.pausePhysics()
        # get obs
        obs = self._get_observation()
        # reset params
        self.step_counter = 0
        rospy.logerr("\nEnvironment Reset!!!\n")

        return obs

    def step(self, act):
        """
        Manipulate scount_1 by giving a skid streering control
        Usage:
            obs, rew, done, info = env.step(act)
        """
        rospy.logdebug("\nStart Environment Step")
        self.unpausePhysics()
        self._take_action(act)
        self.pausePhysics()
        obs = self._get_observation()
        # update status
        reward, done = self._compute_reward()
        info = ''
        self.step_counter += 1
        rospy.logdebug("End Environment Step\n")

        return obs, reward, done, info

    def _get_observation(self):
        pass

    def _take_action(self, act):
        """
        Publish cmd_vel according to an action array
        Args:
            acts: array([lin.x0, ang.z0, lin.x1, ang.z1])
        Returns:
        """
        assert act.shape==self.action_space_shape
        rospy.logdebug("\nStart Taking Action")
        # clip acts
        lin_x = np.clip(act[0], self.action_space_low[0], self.action_space_high[0]) # -1.5 ~ 1.5
        ang_z = np.clip(act[1], self.action_space_low[1], self.action_space_high[1]) # -pi/3 ~ pi/3
        # convert acts to cmd_vels
        cmd_vel = Twist()
        cmd_vel.linear.x = lin_x
        cmd_vel.angular.z = ang_z
        for _ in range(3): # ? Hz
            self.cmd_vel_pub.publish(cmd_vel)
            self.rate.sleep()
        rospy.logdebug("cmd_vel: {}".format(cmd_vel))
        rospy.logdebug("End Taking Action\n")

    def _compute_reward(self):
        """
        Compute reward and done based on current state
        Return:
            reward
            done
        """
        rospy.logdebug("\nStart Computing Reward")
        reward, done = 0, False
        rospy.logdebug("End Computing Reward\n")

        return reward, done


if __name__ == "__main__":
    num_episodes = 4
    num_steps = 1024
    env = ResourceLocate()
    for ep in range(num_episodes):
        env.reset()
        for st in range(num_steps):
            act = env.action_space_high*np.random.randn(2)
            env.step(act)
