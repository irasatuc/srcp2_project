"""
sensor data
"""

import numpy as np
from numpy import pi
import rospy

from srcp2_msgs.msg import VolSensorMsg
from sensor_msgs.msg import Imu, CameraInfo, Image, LaserScan
import cv2
from cv_bridge import cvBridge, cvBridgeError

# camera
# /scout_1/camera/left/camera_info
# /scout_1/camera/left/image_raw
# /scout_1/camera/right/camera_info
# /scout_1/camera/right/image_raw
# imu: /scout_1/imu
# volatile_sensor: /scout_1/volatile_sensor
# laser: /scout_1/laser/scan

class SensorData:
    def __init__(self):
        self.image_shape = () # image (width,height)
        self.caml_image = []
        self.camr_image = []
        self.laser_scan = []
        self.imu = ()
        self.volatile = ()
        self._listen()
        self.bridge = cvBridge()

    def senser_data(self):
        return

    def reset(self):
        self.image_shape = ()
        self.caml_image = []
        self.camr_image = []
        self.laser_scan = []
        self.imu = ()
        self.volatile = ()

    def observation(self):
        obs = dict("left_img":self.caml_image, "right_img":self.camr_image,"imu":self.imu,"laser":self.laser_scan)
        return obs

    def _listen(self):
        rospy.init_node("sensor_data", anonymous=True, log_level=rospy.DEBUG)
        rospy.Subscriber("/scout_1/camera/left/camera_info",CameraInfo, self._caml_info_cb)
        rospy.Subscriber("/scout_1/camera/left/image_raw",Image,self._caml_image_cb)
        rospy.Subscriber("/scout_1/camera/right/camera_info",CameraInfo,self._camr_info_cb)
        rospy.Subscriber("/scout_1/camera/right/image_raw",Image,self._camr_image_cb)
        rospy.Subscriber("/scout_1/imu",Imu,self._imu_cb)
        rospy.Subscriber("/scout_1/laser/scan",LaserScan,self._laser_scan_cb)
        rospy.Subscriber("/scout_1/volatile_sensor",VolSensorMsg,self._volatile_cb)

    def _caml_info_cb(self,data):
        rospy.loginfo("lisetn left camera info")
        print("left camera info",data)
        self.image_shape = (data.width,data.height)

    def _camr_info_cb(self,data):
        rospy.loginfo("lisetn right camera info")
        #print("right camera info",data)
        # shape should be the same as left camere

    def _caml_image_cb(self,data):
        rospy.loginfo("listen left camere image")
        #print("left camere image",data)
        try:
            self.caml_image = self.bridge.imgmsg_to_cv2(data,"bgr8")
        except cvBridgeError as e:
            print(e)

    def _camr_image_cb(self,data):
        rospy.loginfo("listen right camera image")
        #print("right camera image",data)
        try:
            self.camr_image = self.bridge.imgmsg_to_cv2(data,"bgr8")
        except cvBridgeError as e:
            print(e)

    def _imu_cb(self,data):
        rospy.loginfo("listen imu")
        #print("imu",data)
        self.imu = data

    def _laser_scan_cb(self,data):
        rospy.loginfo("listen laser scan")
        print("laser scan",data)
        self.laser_scan = data

    def _volatile_cb(self,data):
        rospy.loginfo("listen volatile")
        print("volatile",data)
        self.volatile = (data.vol_index,data.vol_type,data.shadowed_state,data.distance_to)


if __name__ == "__main__":
    sd = SensorData()
    rospy.spin()
